#!/usr/bin/python
# Note that for each first session (prior to Mosh or SSH), user should use ssh to log into ssh server in order to establish one-time authencity.

from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.log import lg, info
from mininet.util import dumpNodeConnections
from mininet.cli import CLI

from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process
from argparse import ArgumentParser

import termcolor as T

import sys
import os
import math


parser = ArgumentParser(description="Bufferbloat tests")
parser.add_argument('--bw-host', '-B',
                    type=float,
                    help="Bandwidth of host links (Mb/s)",
                    default=1000)

parser.add_argument('--bw-net', '-b',
                    type=float,
                    help="Bandwidth of bottleneck (network) link (Mb/s)",
                    default=1000)

parser.add_argument('--delay',
                    type=str,
                    help="Link propagation delay (ms)",
                    default='50ms')

parser.add_argument('--loss',
                    type=int,
                    help="Loss",
                    default=20)

parser.add_argument('--time', '-t',
                    help="Duration (sec) to run the experiment",
                    type=int,
                    default=1000)

parser.add_argument('--maxq',
                    type=int,
                    help="Max buffer size of network interface in packets",
                    default=100)

# Linux uses CUBIC-TCP by default that doesn't have the usual sawtooth
# behaviour.  For those who are curious, invoke this script with
# --cong cubic and see what happens...
# sysctl -a | grep cong should list some interesting parameters.
parser.add_argument('--cong',
                    help="Congestion control algorithm to use",
                    default="cubic")

# Expt parameters
args = parser.parse_args()

class BBTopo(Topo):

    def __init__(self, n=4):
        super(BBTopo, self).__init__()

        h1 = self.addHost('h1');
        h2 = self.addHost('h2');
        h3 = self.addHost('h3');
        h4 = self.addHost('h4');

        #loss = int(args.loss)
        #i can't get arg passing to work for some reason 
        print ("Link delay is %s and loss is %f percent.\n"%(args.delay, args.loss))
        self.addLink(h1, h2, bw=1000, delay=args.delay,loss=args.loss);
        self.addLink(h3, h4, bw=1000, delay=args.delay,loss=args.loss);
        return


def sshd(net, cmd='/usr/sbin/sshd', opts=' ' ):
    for host in net.hosts:
        host.cmd( cmd + ' ' + opts + '&' )
    print
    print "*** Hosts are running sshd at the following addresses:"
    print
    for host in net.hosts:
        print host.name, host.IP()
    print
    print "*** Type 'exit' or control-D to shut down network"
    for host in net.hosts:
        host.cmd( 'kill %' + cmd )



def mosh_emulate():
    os.system("sysctl -w net.ipv4.tcp_congestion_control=%s" % args.cong)
    topo = BBTopo()
    net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
    net.start()
    # This dumps the topology and how nodes are interconnected through
    # links.
    dumpNodeConnections(net.hosts)
    # This performs a basic all pairs ping test.
    #net.pingAll()

    start_time = time()
    #sshd(net)
    sleep(2);
    # started ssh servers for both 
    h1 = net.getNodeByName('h1');     
    h2 = net.getNodeByName('h2');     
    h3 = net.getNodeByName('h3');     
    h4 = net.getNodeByName('h4');     

    h1IP = h1.IP(); 
    h3IP = h3.IP(); 
    h1.popen("/usr/sbin/sshd", shell=True);
    h3.popen("/usr/sbin/sshd", shell=True);
    
    
    print("Trying to run ssh from %s to %s\n"%(h4.IP(), h3IP));

    moshcmd = "perl /home/ubuntu/mosh_with_mininet/stm-data-mod/term-replay-client /home/ubuntu/mosh_with_mininet/original_logs/sample_short.keys mosh --predict=never %s \"perl /home/ubuntu/mosh_with_mininet/stm-data-mod/term-replay-server /home/ubuntu/mosh_with_mininet/original_logs/sample_short.keys\" 2> /home/ubuntu/mosh_with_mininet/user_replayed_logs/sample_50ms-1way-delay_%d-loss_mosh.replay > /home/ubuntu/output" %(h1IP,args.loss)
    sshcmd = "perl /home/ubuntu/mosh_with_mininet/stm-data-mod/term-replay-client /home/ubuntu/mosh_with_mininet/original_logs/sample_short.keys ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null %s \"perl /home/ubuntu/mosh_with_mininet/stm-data-mod/term-replay-server /home/ubuntu/mosh_with_mininet/original_logs/sample_short.keys\" 2> /home/ubuntu/mosh_with_mininet/user_replayed_logs/sample_50ms-1way-delay_%d-loss_ssh.replay > /home/ubuntu/output" %(h3IP, args.loss)

    sshdummycmd = "ssh -o StrictHostKeyChecking=no %s \"/usr/bin/touch /tmp/waste\""%(h1IP)
    #print("Running %s\n"%moshcmd);

    # Runing mosh
    if(os.path.exists("/tmp/mosh_done")):
        os.remove("/tmp/mosh_done");
    pop1 = h4.popen(sshcmd, shell=True);
    checked = 0;
    while((os.path.exists("/tmp/mosh_done") == False) and (checked < 300)):
        print(".");
        sleep(1);

    if(checked == 900):
        print("ssh might have died due to excess delays.. please ignore output and restart the script\n");
        sys.exit()
    print("ssh finished.. moving on\n");
    h2.popen(sshdummycmd);
    print("ran dummy ssh command to fix key issue\n");
    print("Trying to run mosh from %s to %s\n"%(h2.IP(), h1IP));

    if(os.path.exists("/tmp/mosh_done")):
        os.remove("/tmp/mosh_done");
    pop1 = h2.popen(moshcmd, shell=True);
    checked = 0;
    while((os.path.exists("/tmp/mosh_done") == False) and (checked < 300)):
        print(".");
        sleep(1);
    if(checked == 900):
        print("mosh might have died due to excess delays.. please ignore output and restart the script\n");
        sys.exit()

    print("Mosh Process finished.. exiting\n");
        
    net.stop();

if __name__ == "__main__":
    mosh_emulate()
