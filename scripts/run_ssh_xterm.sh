#!/bin/bash
# init
function pause(){
   read -p "$*"
}

pause 'For one-time SSH authentication between h1 and h2, we SSH into h1 server [press enter]'
pause 'After you have successfully logged into h1, type "exit" to terminiate SSH session in h1 and return to h2 [press enter]' 
ssh 10.0.0.1

echo "\n Back in h2 \n"
pause 'SSH into host with file for play back... please note 20 - 30 sec delay [press enter]' 
pause 'Also note that due to random nature of losses on emulated link, the Mosh command may not be successful.  If that happens, SSH will exit and you should repeat this script. [press enter]'

perl ~/mosh_with_mininet/stm-data-mod/term-replay-client ~/mosh_with_mininet/original_logs/sample_short.keys ssh 10.0.0.1 "perl /home/ubuntu/mosh_with_mininet/stm-data-mod/term-replay-server /home/ubuntu/mosh_with_mininet/original_logs/sample_short.keys" 2> /home/ubuntu/mosh_with_mininet/user_replayed_logs/sample_50ms-1way-delay_29-loss_ssh.replay

