#!/bin/bash
# init
function pause(){
   read -p "$*"
}

echo "Alg  | Delay | Loss | Median | Ave  | Stdev | num_chars"; 
 
for i in `ls ~/mosh_with_mininet/user_replayed_logs/`; do ~/mosh_with_mininet/scripts/parse_results.py $i; done
#>> ~/mosh_with_mininet/user_parsed_results/delay50ms_results; done

#pause 'Print out results for 50ms delay [press enter]'
#cat ~/mosh_with_mininet/user_parsed_results/delay50ms_results
