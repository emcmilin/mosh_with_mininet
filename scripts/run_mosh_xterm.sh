#!/bin/bash
# init
function pause(){
   read -p "$*"
}

pause 'For one-time SSH authentication between h3 and h4, we SSH into h3 server [press enter]'
pause 'After you have successfully logged into h3, type "exit" to terminiate SSH session in h3 and return to h4 [press enter]'
ssh 10.0.0.3

echo "\n Back in h4 \n"
pause 'SSH into host with file for play back... please note 20 - 30 sec delay [press enter]'
pause 'Also note that due to random nature of losses on emulated link, the Mosh command may not be successful.  If that happens, SSH will exit and you should repeat this script. [press enter]'

perl ~/mosh_with_mininet/stm-data-mod/term-replay-client ~/mosh_with_mininet/original_logs/sample_short.keys mosh --predict=never 10.0.0.3 "perl /home/ubuntu/mosh_with_mininet/stm-data-mod/term-replay-server /home/ubuntu/mosh_with_mininet/original_logs/sample_short.keys" 2> /home/ubuntu/mosh_with_mininet/user_replayed_logs/sample_50ms-1way-delay_29-loss_mosh.replay

