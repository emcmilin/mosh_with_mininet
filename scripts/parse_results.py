#!/usr/bin/python
# run "sudo port install py-numpy" on mac
import numpy
import sys
import re
filename = sys.argv[1]
dirname="/home/ubuntu/mosh_with_mininet/user_replayed_logs/"
f = open(dirname+filename, 'r');

#feed the file text into findall(); it returns a list of all the found strings
list_delays_str = re.findall(r'\d*\.\d*', f.read())

f.close

#change from list of strings to list of floats
list_delays_f = [float(x) for x in list_delays_str]

#pop off the first two which are always outliers
list_delays_f.pop(0)
list_delays_f.pop(0)

#change from list of floats into an array of floats for numpy
delays = numpy.asarray(list_delays_f, dtype=numpy.float)

# pull out file values
file_vals = re.search('sample_(\d+)ms-1way-delay_(\d+)-loss_(\w+).replay', filename)
delay_val = int(file_vals.group(1))	
loss_val = int(file_vals.group(2))		
alg = file_vals.group(3)			

#compute and print stuff
length = len (delays)
ave = numpy.mean (delays)
median = numpy.median (delays)
stdev = numpy.std(delays)
#print ("median is %f, ave is %f, and stdev is %f. length is %f\n"%(median, ave, stdev, length))

# write parsed values 50ms delay values to files
#dirname="/home/ubuntu/mosh_with_mininet/user_parsed_results/"
#fname="delay50ms_results"
# erases the file if it already exist
#f=open(dirname+fname, 'w');
#f.close()

if delay_val == 50:
#   f=open(dirname+fname, 'a');
#   f.write ("alg, delay, loss, median, ave, stdev, num_chars\n")
#   f.write ("%s %d %d %f %f %f %d\n" % (alg, delay_val, loss_val, median, ave, stdev, length))
#   print "\nalg, delay, loss, median, ave, stdev, num_chars\n"
   if(alg == "ssh"):
        alg = "ssh"+" "      
   print "%s | %d    |  %d  | %.4f |%.4f|%.4f | %d\n" % (alg, delay_val, loss_val, median, ave, stdev, length)


#if loss_val == 10:
#   fname="loss10%_results"
#   f=open(dirname+fname, 'a');
#   f.write ("%s %d %d %f %f %f %d\n" % (alg, delay_val, loss_val, median, ave, stdev, length))
f.close()
