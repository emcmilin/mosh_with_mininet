#!/bin/bash
# init
function pause(){
   read -p "$*"
}

pause 'Because mininet is run from root, we must generate a key for root [press enter]'
pause 'Select default filename and no passphrase because the secure login will be automated [press enter]'
 
sudo ssh-keygen -t rsa -b 1024

echo ''
pause 'Adding your key to authorized keys file for root [press enter]'
sudo cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys2

echo ''
pause 'Removing strict host key checking from SSH config file [press enter]'
sudo echo StrictHostKeyChecking no >> /etc/ssh/ssh_config

echo ""
pause 'Installing mosh [press enter]'
sudo apt-get -y install mosh

echo ""
pause 'Installing libs to run Moshs key-save and key-replaying scripts [press enter]'
sudo apt-get install libio-pty-easy-perl

echo "Setup Done.. Please run mosh_with_mininet/scripts/run.sh"

#echo ""
#pause 'Running our mininet set-up with the default values of --delay = 50ms and --loss = 29% [press enter]'
#sudo python ~/mosh_with_mininet/scripts/mosh_emulate.py --delay 50 --loss 29
