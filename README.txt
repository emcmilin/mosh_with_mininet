# Log into a new instance of using the CS244-Win13-Mininet AMI.
# Please use our class's established default settings of 
# c1.medium and security setting of quicklaunch-1.
#
# Note that for more rapid program completion time
# this test is run on a sub-sample of our keylog file.
# (49 of the of total 2966 keystrokes logged)

# Use -Y for x11 if CLI is desired
ssh -Y ubuntu@ec2-xx-xxx-xxx-xxx.us-west-2.compute.amazonaws.com

# Now you are in the EC2 main terminal
# Clone mosh_with_mininet
git clone https://emcmilin@bitbucket.org/emcmilin/mosh_with_mininet.git

# Run set-up
sudo ./mosh_with_mininet/scripts/run_setup.sh 

# Run the code
sudo ./mosh_with_mininet/scripts/run.sh

# Please note that we set the default loss rate to 20% in order to 
# greatly reduce overall completion time. However we held the 
# default delay to 50ms.
#
# Any other loss rate can be selected by opening file:
#
# mosh_with_mininet/scripts/run.sh
#
# and editing line 2, to pass your selected one way loss rate (x)
# as shown below:
#
# sudo python ~/mosh_with_mininet/scripts/mosh_emulate.py --delay 50 --loss x
# 
# With delay set to 50ms, results for link loss rate (one way) varied 
# from 0 to 29% can be compared to our top two charts.
